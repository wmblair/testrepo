﻿$(document).ready(function () {

        $('#AddNewDvdForm')
            .validate(
            {
                rules:
                {
                    "Dvd.ReleaseDate":
                    {
                        required: true,
                        date:true
                    },

                    "Dvd.Title":
                    {
                        required: true
                    },

                    "Studio.StudioName":
                    {
                        required: true
                    },

                    "MpaaRating.MpaaRatingId":
                    {
                        required: true
                    }
                },
                messages: {
                    "Dvd.ReleaseDate":
                    {
                        required: "Please enter a date"
                    },

                    "Dvd.Title":
                    {
                        required: "Please enter a title"
                    },

                    "Studio.StudioName":
                    {
                        required: "Please enter a studio name"
                    },

                    "MpaaRating.MpaaRatingId":
                    {
                        required: "Please enter a rating"
                    }
                }

            });
        });

