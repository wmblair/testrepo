﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models.View_Models
{
    public class DvdViewModel
    {
        public Dvd Dvd { get; set; }
        public MpaaRating MpaaRating { get; set; }
        public Studio Studio { get; set; }
    }
}