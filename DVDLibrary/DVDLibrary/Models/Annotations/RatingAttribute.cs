﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models.Annotations
{
    public class RatingAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            int? rating = value as int?;
            return rating > 0;
        }
    }
}
