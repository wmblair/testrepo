﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVDLibrary.Models
{
    public class DvdPersonnel
    {
        public int DvdId { get; set; }
        public int PersonnelId { get; set; }
        public int RoleId { get; set; }
    }
}