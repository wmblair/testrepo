﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.Models.Annotations;

namespace DVDLibrary.Models
{
    public class MpaaRating
    {
     //   [Rating(ErrorMessage = "Please enter a valid rating")]
        public int MpaaRatingId { get; set; }

        [Display(Name="MPAA Rating")]
        public string MpaaRatingName { get; set; }

        public List<SelectListItem> RatingItems { get; set; }

        public MpaaRating()
        {
            RatingItems = new List<SelectListItem>();
        }

        public void SetRatingItems(IEnumerable<MpaaRating> ratings)
        {
            foreach (var rating in ratings)
            {
                RatingItems.Add(new SelectListItem()
                {
                    Value = rating.MpaaRatingId.ToString(),
                    Text = rating.MpaaRatingName
                });
            }
        }
    }
}