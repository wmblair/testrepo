﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.Models
{
    public class Studio
    {
        public int StudioId { get; set; }

        [Display(Name="Studio Name")]
      //  [Required(ErrorMessage = "Please enter a studio that produced this movie")]
        public string StudioName { get; set; }

        public List<SelectListItem> StudioItems { get; set; }

        public Studio()
        {
            StudioItems = new List<SelectListItem>();
        }

        public void SetStudioItems(IEnumerable<Studio> studios)
        {
            foreach (var studio in studios)
            {
                StudioItems.Add(new SelectListItem()
                {
                    Value = studio.StudioId.ToString(),
                    Text = studio.StudioName
                });
            }
        }
    }
}