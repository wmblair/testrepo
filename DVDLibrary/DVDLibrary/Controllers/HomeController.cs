﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.Models;
using DVDLibrary.Models.View_Models;
using DVDLibrary.Repositories;

namespace DVDLibrary.Controllers
{
    public class HomeController : Controller
    {       
        public ActionResult Index()
        {
            return View();
        }  

        public ActionResult DisplayDvd(int id)
        {
            DvdRepository repo = new DvdRepository();
            Dvd dvd = repo.GetDvdById(id);
            return View(dvd);
        }

        public ActionResult DisplayAllDvds()
        {
            DvdRepository repo = new DvdRepository();
            List<DvdViewModel> dvds = repo.GetAllDvds();
            return View(dvds);
        }

        public ActionResult SearchForDvds(string search)
        {
            DvdRepository repo = new DvdRepository();
            List<DvdViewModel> dvds = repo.SearchForDvds(search);
            return View(dvds);
        }

        public ActionResult RemoveDvd(int id)
        {
            DvdRepository repo = new DvdRepository();
            repo.RemoveDvd(id);
            return RedirectToAction("DisplayAllDvds");
        }

        public ActionResult AddNewDvd()
        {
            DvdRepository repo = new DvdRepository();

            DvdViewModel dvdvm = new DvdViewModel
            {
                MpaaRating = new MpaaRating()
            };
            List<MpaaRating> ratingsList = repo.GetAllRatings();
            dvdvm.MpaaRating.SetRatingItems(ratingsList);

            return View(dvdvm);
        }

        [HttpPost]
        public ActionResult AddNewDvd(DvdViewModel dvdvm)       //you want to keep validation local so you don't have to wait for the server to fullfill post and another get request in order to see validation
        {
        //    dvdvm.Dvd.MpaaRatingId = dvdvm.MpaaRating.MpaaRatingId;
            DvdRepository repo = new DvdRepository();                      

       //     dvdvm.MpaaRating = new MpaaRating();

            List<MpaaRating> ratingsList = repo.GetAllRatings();
            dvdvm.MpaaRating.SetRatingItems(ratingsList);

            repo.AddNewDvd(dvdvm);            

           //var errors = ModelState
           //     .Where(x => x.Value.Errors.Count > 0)
           //     .Select(x => new {x.Key, x.Value.Errors})
           //     .ToArray();

            if (ModelState.IsValid)
            {
                return RedirectToAction("DisplayAllDvds");
            }
            return View(dvdvm);
        }

        public ActionResult DisplayBorrowerForDvd(int id)
        {
            DvdRepository repo = new DvdRepository();
            List<BorrowerViewModel> bvm = repo.DisplayBorrowersForDvd(id);
            return View(bvm);
        }

        //public ActionResult DisplayAllBorrowers(int id)
        //{
        //    DvdRepository repo = new DvdRepository(); 
        ////    List<DvdBorrower> DvdBorrowerList = repo.GetAllBorrowers(id);

        //    return View(DvdBorrowerList);
        //}

    }
}