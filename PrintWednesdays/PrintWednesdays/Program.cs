﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWednesdays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a future date: ");
            string dateInput = Console.ReadLine();
            DateTime future = new DateTime();
            DateTime.TryParse(dateInput, out future);
            DateTime today = DateTime.Now;

            TimeSpan difference = new TimeSpan();
            difference = future - today;
            int num = difference.Days;

            Console.WriteLine(difference);

            DayOfWeek w1 = new DayOfWeek();

            int count = 0;

            for (int i = 1; i < num+1; i++)
            {
                if (i%7 == 0)
                w1 = DayOfWeek.Sunday;
                if (w1 == DayOfWeek.Wednesday)
                {
                    count++;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("w1: " + w1.ToString());
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                    Console.WriteLine("w1: " + w1.ToString());
                w1++;
            }
            Console.WriteLine("Number of Wednesdays: " + count);
            Console.ReadLine();
        }
    }
}
