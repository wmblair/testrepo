﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextPrimeNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            EnterPrime();
        }

        public static void EnterPrime()
        {
            string input = "";
            do
            {
            Console.WriteLine("Enter a number to determine the next prime number (Press Q to quit): ");
            input = Console.ReadLine();
            int intInput = 0;

                if (input == "q" || input == "q".ToUpper())
                    break;

                    while (!int.TryParse(input, out intInput))
                    {
                        Console.WriteLine("Please enter a number: ");
                        input = Console.ReadLine();
                    }

            int i = intInput;

            if (i % 2 == 0)
                i--;

            int j;

            do
            {
                i+=2;
                for (j = 3; j <= (i/2); j+=2)
                {
                    if (i % j == 0)
                        break;                    
                }
            }
            while (j < (i/2));

            Console.WriteLine("The next prime number from " + intInput + " is " + i);
            }
            while (true);
        }
    }
}
