﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MazePathfinder.UI
{
    public static class Maze
    {
        private static List<MazeLocation> _resultArray;
        private static string _text;

        static void Main(string[] args)
        {
            _resultArray = new List<MazeLocation>();

            Console.WriteLine("Welcome to the maze pathfinder! Select an option below to begin:");
            Console.WriteLine("-------------------------------");
            Console.WriteLine("1) Read maze from file");
            Console.WriteLine("2) Create maze");

            MazeLocation[,] maze = null;
            MazeLocation[,] maze2 = null;
            MazeLocation[,] maze3 = null;
            string input = "";

            do
            {
                input = Console.ReadLine();

                if (input == "1")
                {
                    maze = ReadFromFile();
                    maze2 = new MazeLocation[maze.GetLength(0), maze.GetLength(1)];
                    maze3 = new MazeLocation[maze.GetLength(0), maze.GetLength(1)];
                    WriteIntoArray(maze);
                    break;
                }
                if (input == "2")
                {
                    maze = CreateMaze();
                    maze2 = new MazeLocation[maze.GetLength(0), maze.GetLength(1)];
                    maze3 = new MazeLocation[maze.GetLength(0), maze.GetLength(1)];
                    WriteIntoArray(maze);
                    break;
                }
            }
            while (true);

            maze2 = CopyArrayValue(maze, maze2);
            maze3 = CopyArrayValue(maze, maze3);

            PrintMaze(maze);

            Console.Write("\n\nFrom A:");
            FindPath(0, maze.GetLength(1)-1, maze, _resultArray); //start from A

            Console.Write("\n\nFrom B:");
            FindPath(maze2.GetLength(0)-1, 0, maze2, _resultArray); //start from B

            Console.Write("\n\nFrom C:");
            FindPath(maze3.GetLength(0)-1, maze3.GetLength(1)-1, maze3, _resultArray); //start from C

            Console.ReadLine();
        }

        public static MazeLocation[,] CreateMaze()
        {
            Console.WriteLine("Enter the dimensions of the maze you want to create: ");
            string dimensions = Console.ReadLine();
            string rows = "", cols;
            int i = 0, rowsInt = 0, colsInt = 0;

            for (i = 0; i < dimensions.Length; i++)
            {
                if (dimensions[i] == ' ' || dimensions[i] == ',')
                {
                    rows = dimensions.Substring(0, i);
                    break;
                }
            }

            cols = dimensions.Substring(i + 1);

            int.TryParse(rows, out rowsInt);
            int.TryParse(cols, out colsInt);

            Console.WriteLine("Type your maze of dimensions " + rows + ", " + cols + 
            " in the form of 1's (path) and 0's (wall)");

            for (int j = 0; j < rowsInt; j++)
            {
                string line = Console.ReadLine();
                _text += line;

                if (line.Length > colsInt)
                {
                    Console.WriteLine("The number of columns you entered is too big. Try again");
                    j--;
                }
            }         
            MazeLocation[,] array = new MazeLocation[rowsInt,colsInt];

            return array;
        }

        public static MazeLocation[,] ReadFromFile()
        {
            _text = "";
            int rowCount = 0;
            int colCount = 0;

            try
            {
                using (StreamReader sr = new StreamReader("TestData.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        _text += line;
                        rowCount++;
                        colCount = line.Length;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            MazeLocation[,] ml = new MazeLocation[rowCount,colCount];

            return ml;
        }

        public static MazeLocation[,] WriteIntoArray(MazeLocation[,] array)
        {        
            int count = 0;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array[i,j] = new MazeLocation()
                    {
                        Value = int.Parse(_text[count++].ToString()),
                        Visited = false,
                        direction = LocationDirection.NotChecked
                    };
                }
            }
            return array;
        }

        public static MazeLocation[,] CopyArrayValue(MazeLocation[,] array, MazeLocation[,] array2)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array2[i,j] = new MazeLocation()
                    {
                        Value = array[i,j].Value,
                        Visited = false,
                        direction = LocationDirection.NotChecked
                    };
                }
            }
            return array2;
        }

        public static void PrintMaze(MazeLocation[,] maze)
        {
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    if (maze[i, j].direction != LocationDirection.CheckedAll &&
                        maze[i, j].direction != LocationDirection.NotChecked)
                        Console.Write(" -");
                    else if(i == 0 && j == maze.GetLength(1)-1)
                        Console.Write(" A");
                    else if(i == maze.GetLength(0)-1 && j == 0)
                        Console.Write(" B");
                    else if(i == maze.GetLength(0)-1 && j == maze.GetLength(1)-1)
                        Console.Write(" C");
                    else
                        Console.Write(" " + maze[i, j].Value);

                }
            }
        }
        
        public static void RemoveBadPaths(List<MazeLocation> listArray)
        {
            for (int i = 0; i < listArray.Count; i++)
            {
                if (listArray[i].direction == LocationDirection.CheckedAll
                    || listArray[i].direction == LocationDirection.NotChecked)
                {
                    listArray.RemoveAt(i);
                    i--;
                }
            }
        }

        public static void PrintResults(List<MazeLocation> listArray)
        {
            Console.WriteLine();
            int count = 1;
            for (int i = 0; i < listArray.Count - 1; i++)
            {
                if (listArray[i].direction == listArray[i + 1].direction)
                    count++;
                else
                {
                    Console.Write(count + "" + listArray[i].direction.ToString());
                    count = 1;
                }
            }
        }

        public static bool CheckUp(int r, int c, MazeLocation[,] array)
        {
            if (r != 0 && array[r - 1, c].Value == 1)
            {
                if(!array[r - 1, c].Visited)
                return true;
            }
            return false; 
        }

        public static bool CheckRight(int r, int c, MazeLocation[,] array)
        {
            if (c != array.GetLength(1) - 1 && array[r, c + 1].Value == 1)
            {
                if (!array[r, c + 1].Visited)
                    return true;
            }
            return false;
        }

        public static bool CheckDown(int r, int c, MazeLocation[,] array)
        {
            if (r != array.GetLength(0) - 1 && array[r + 1, c].Value == 1)
            {
                if (!array[r + 1, c].Visited)
                    return true;
            }
            return false;
        }

        public static bool CheckLeft(int r, int c, MazeLocation[,] array)
        {
            if (c != 0 && array[r, c - 1].Value == 1)
            {
                if (!array[r, c - 1].Visited)
                    return true;
            }
            return false;
        }

        public static void FindPath(int r, int c, MazeLocation[,] array, List<MazeLocation> listArray)
        {
           
            if (r == array.GetLength(0) - 1 && c == array.GetLength(1) - 1) //starting points (A,B,C) won't be included when directions are printed out...
            {                                                         //...if they aren't marked as any direction and haven't been added to resultArray
                array[r, c].direction = LocationDirection.U;
                listArray.Add(array[r, c]);
            }
            else if (r == 0 && c == array.GetLength(1) - 1)
            {
                array[r, c].direction = LocationDirection.U;
                listArray.Add(array[r, c]);
            }
            else if (r == array.GetLength(0) - 1 && c == 0)
            {
                array[r, c].direction = LocationDirection.U;
                listArray.Add(array[r, c]);
            }
            
            if (r == 0 && c == 0) {         //check if end has been found
                array[r, c].direction = LocationDirection.U;

                RemoveBadPaths(listArray);

                PrintMaze(array);
                PrintResults(listArray);
            }
            
                if (CheckUp(r, c, array))    //check top for more available locations
                {
                    listArray.Add(array[r-1, c]);
                    array[r, c].Visited = true;
                    array[r - 1, c].Visited = true;
                    array[r, c].direction = LocationDirection.U;                    
                    FindPath(r - 1, c, array, listArray);
                }

                if (CheckRight(r, c, array))    //right
                {
                    listArray.Add(array[r, c+1]);
                    array[r, c].Visited = true;
                    array[r, c + 1].Visited = true;
                    array[r, c].direction = LocationDirection.R;
                    FindPath(r, c + 1, array, listArray);
                }

                if (CheckDown(r, c, array))     //down
                {
                    listArray.Add(array[r+1, c]);
                    array[r, c].Visited = true;
                    array[r + 1, c].Visited = true;
                    array[r, c].direction = LocationDirection.D;
                    FindPath(r + 1, c, array, listArray);
                }

                if (CheckLeft(r, c, array))     //left
                {
                    listArray.Add(array[r, c-1]);
                    array[r, c].Visited = true;
                    array[r, c - 1].Visited = true;
                    array[r, c].direction = LocationDirection.L;
                    FindPath(r, c - 1, array, listArray);
                }

                if (!CheckUp(r, c, array) && !CheckRight(r, c, array) &&
                !CheckDown(r, c, array) && !CheckLeft(r, c, array))
                    array[r, c].direction = LocationDirection.CheckedAll;
        }
    }
}
