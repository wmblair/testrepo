﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder
{
    public enum LocationDirection
    {
        NotChecked,
        U,
        R,
        D,
        L,
        CheckedAll
    }
}
