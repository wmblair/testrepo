﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazePathfinder
{
    public class MazeLocation
    {
        public int Value { get; set; }
        public bool Visited { get; set; }
        public LocationDirection direction { get; set; }
   
        //public int directionCount { get; set; }
    }
}
