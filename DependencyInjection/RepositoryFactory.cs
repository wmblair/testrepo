﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SGCorp.Data
{
    public static class RepositoryFactory
    {
        public static IOrderRepository RepositorySelection()
        {
           var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockOrderRepository();
                default:
                    return new OrderRepository();
            }           
        }
    }
}
