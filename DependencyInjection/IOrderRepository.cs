﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SGCorp.Models;

namespace SGCorp.Data
{
    public interface IOrderRepository
    {
        List<Order> DisplayOrders(DateTime date);

        Order CreateOrder(DateTime date, Order order);

        void RemoveOrder(DateTime date, int orderId);

        Order EditOrder(DateTime date, Order order);

        Order LoadOrder(DateTime date, int orderId);

        void WriteToErrorFile(string ex);

        //  Order EditOrder();
        //  Order DeactivateOrder();

    }
    //public List<Order> DisplayOrders(List<Order> list, string date)
    //public Order CreateOrder(List<Order> list, string date, Order order)
    //public void WriteToFile(List<Order> list, string date)
    //public string GetTextFile(string date)
    //
}
